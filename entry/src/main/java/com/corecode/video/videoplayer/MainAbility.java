package com.corecode.video.videoplayer;

import ohos.ace.ability.AceAbility;
import ohos.aafwk.content.Intent;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;
import ohos.utils.zson.ZSONObject;

import java.util.List;

public class MainAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"},0);
        super.onStart(intent);
        DeviceInternalAbility.register(this);
    }

    public static String getList(){
        List<DeviceInfo> deviceInfos = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
         return ZSONObject.toZSONString(deviceInfos);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
