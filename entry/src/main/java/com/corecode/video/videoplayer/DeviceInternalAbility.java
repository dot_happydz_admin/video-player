package com.corecode.video.videoplayer;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.ace.ability.AceInternalAbility;
import ohos.app.AbilityContext;
import ohos.app.Context;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;
import ohos.rpc.IRemoteObject;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.MessageOption;
import ohos.rpc.MessageParcel;
import ohos.rpc.RemoteException;
import ohos.utils.zson.ZSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceInternalAbility extends AceInternalAbility {
    private static final HiLogLabel TAG = new HiLogLabel(0, 0, "DeviceInternalAbility");
    private static final int CONNECT_ABILITY = 2000;
    private static final int DISCONNECT_ABILITY = 2001;
    private static final int SEND_MSG = 1001;
    private static final int SUCCESS_CODE = 0;
    private static final int FAIL_CODE = -1;
    private static DeviceInternalAbility INSTANCE;
    private String selectDeviceId;

    /**
     * default constructor
     *
     * @param context ability context
     */
    public DeviceInternalAbility(AbilityContext context) {
        super("com.corecode.video.videoplayer", "com.corecode.video.videoplayer.DeviceInternalAbility");
    }

    public DeviceInternalAbility(String bundleName, String abilityName) {
        super(bundleName, abilityName);
    }

    public DeviceInternalAbility(String abilityName) {
        super(abilityName);
    }

    /**
     * setInternalAbilityHandler for DistributeInternalAbility instance
     *
     * @param context ability context
     */
    static void register(AbilityContext context) {
        INSTANCE = new DeviceInternalAbility(context);
        INSTANCE.setInternalAbilityHandler((code, data, reply, option) ->
                INSTANCE.onRemoteRequest(code, data, reply, option));
    }

    /**
     * destroy DistributeInternalAbility instance
     */
    private static void unregister() {
        INSTANCE.destroy();
    }

    /**
     * default destructor
     */
    private void destroy() {
    }

    /**
     * hand click request from javascript
     *
     * @param code   ACTION_CODE
     * @param data   data sent from javascript
     * @param reply  reply for javascript
     * @param option currently excessive
     * @return whether javascript click event is correctly responded
     */
    private boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) {
        Map<String, Object> replyResult = new HashMap<>();
        switch (code) {
            // send message to remote device, message contains controller command from FA
            case SEND_MSG: {
                ZSONObject dataParsed = ZSONObject.stringToZSON(data.readString());
                int message = dataParsed.getIntValue("message");
//                // SYNC
//                if (option.getFlags() == MessageOption.TF_SYNC) {
//                    reply.writeString(ZSONObject.toZSONString(result));
//                }
                // ASYNC
                // 返回结果当前仅支持String，对于复杂结构可以序列化为ZSON字符串上报
                Map<String, Object> result = new HashMap<String, Object>();
                result.put("result", MainAbility.getList());
                MessageParcel responseData = MessageParcel.obtain();
                responseData.writeString(ZSONObject.toZSONString(result));
                IRemoteObject remoteReply = reply.readRemoteObject();
                try {
                    remoteReply.sendRequest(0, responseData, MessageParcel.obtain(), new MessageOption());
                } catch (RemoteException exception) {
                    return false;
                } finally {
                    responseData.reclaim();
                }
                break;
            }
            // to invoke remote device's newsShare ability and send news url we transfer
            case CONNECT_ABILITY: {
                ZSONObject dataParsed = ZSONObject.stringToZSON(data.readString());
                selectDeviceId = dataParsed.getString("deviceId");
//                boolean result = RemoteDataSender.connectRemote(selectDeviceId, context);
//                assembleReplyResult(result ? SUCCESS_CODE : FAIL_CODE, replyResult, new Object(), reply);
                break;
            }
            // when controller FA went to destroy lifeCycle, disconnect with remote newsShare ability
            case DISCONNECT_ABILITY: {
//                RemoteDataSender.disConnectRemote(selectDeviceId, context);
//                assembleReplyResult(SUCCESS_CODE, replyResult, new Object(), reply);
                unregister();
                break;
            }
            default:
                HiLog.error(TAG, "messageCode not handle properly in phone distributeInternalAbility");
        }
        return true;
    }

    private void assembleReplyResult(int code, Map<String, Object> replyResult, Object content, MessageParcel reply) {
        replyResult.put("code", code);
        replyResult.put("content", content);
        reply.writeString(ZSONObject.toZSONString(replyResult));
    }
}