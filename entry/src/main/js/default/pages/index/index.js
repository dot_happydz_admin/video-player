import prompt from '@system.prompt';

export default {
    data: {
        title: "",
        player_index: 0,
        time: 0,
        ifhidden: 'hidden',
        guanzu: 'visible',
        devicelist: 'hidden',
        list: [{
                   path: "/common/video/video1.mp4"
               }
        , {
                   path: "/common/video/video2.mp4"
               }
        , {
                   path: "/common/video/video3.mp4"
               }
        , {
                   path: "/common/video/video4.mp4"
               }
        , {
                   path: "/common/video/video5.mp4"
               }
        , {
                   path: "/common/video/video6.mp4"
               }],
        todolist: [{
                       name: '青铜',
                       detail: '参加活动，赢取重磅好礼！',
                   }, {
                       name: '白金',
                       detail: '这个视频不错，支持支持。',
                   }],
        todolist2: [],
        deviceId: [],
        message: '',
        content: '',
        placecontent: "请输入信息",
        scrollAmount: 30,
        loop: 3,
        marqueeDir: 'left',
        marqueeCustomData: 'Custom marquee',
        zan: 'zan',
        deviceinfo: ''
    },
    onInit() {
        this.title = this.$t('strings.world');
    },
    onShow() {
        //        this.$element('customMarquee').start();
    },
    changeVideo(obj) {
        this.ifhidden = 'hidden';
        this.devicelist = "hidden";
        this.$element(`playerId${this.player_index}`).pause();
        this.player_index = obj.index;
        this.$element(`playerId${this.player_index}`).start();
    },
    switch_equipment() {
        this.getLevel();
    },
    continueVideoAbility: async function () {
        let conti = await FeatureAbility.continueAbility();
        console.log("continueAbility=====11" + JSON.stringify(conti))
        console.log("continueAbility=====22" + this.deviceinfo)
        this.devicelist = "hidden";
    },
    enterdevice(e){
        this.deviceinfo = e;
        this.continueVideoAbility();
    },
    onSaveData(saveData) {
        // 数据保存到savedData中进行迁移。
        var data = {
            list: this.list,
            player_index: this.player_index,
        };
        Object.assign(saveData, data)
    },
    onRestoreData(restoreData) {
        // 收到迁移数据，恢复。
        console.info('==== onRestoreData ' + JSON.stringify(restoreData))
        this.list = restoreData.list
        this.player_index = restoreData.player_index

        this.$element('swiper').swipeTo({
            index: this.player_index
        });

        this.$element(`playerId${this.player_index}`).start();
        //        this.isStart = restoreData.isStart
        //        if (this.isStart) {
        //            this.$element(`videoId${this.playIndex}`).start();
        //        } else {
        //            this.$element(`videoId${this.playIndex}`).pause();
        //        }
    },
    clickzan() {
        if (this.zan == "zan") {
            this.zan = 'zanpress';
        } else {
            this.zan = 'zan';
        }
    },
    onCompleteContinuation(code) {
        console.log("onCompleteContinuation===" + code)
    },
    onStartContinuation() {
        return true;
    },
    pinglun() { //评论
        this.ifhidden = 'visible';
    },
    hideenbg() { //点击头部消失
        this.ifhidden = 'hidden';
    },
    change(e) {
        this.message = e.value;
        console.log("message===" + this.message)
    },
    guanzhu(e) {
        prompt.showToast({
            message: "关注成功",
            duration: 1000,
        });
        this.guanzu = 'hidden';
    },
    sendmessage() {
        this.todolist.push({
            name: '王者',
            detail: this.message,
        })
        this.content = "";
        this.message = "";
    },
    share(e) {
        this.$element('simpledialog').show()
    },
    cancelDialog(e) {
        prompt.showToast({
            message: '分享取消'
        })
    },
    cancelSchedule(e) {
        this.$element('simpledialog').close()
        prompt.showToast({
            message: '分享取消'
        })
    },
    setSchedule(e) {
        this.$element('simpledialog').close()
        prompt.showToast({
            message: '确定分享'
        })
    },
    preparedCallback: function (e) {
        this.event = '视频连接成功';
        this.duration = e.duration;
    },
    startCallback: function () {
        this.event = '视频开始播放';
    },
    pauseCallback: function () {
        this.event = '视频暂停播放';
    },
    finishCallback: function () {
        this.event = '视频播放结束';
    },
    errorCallback: function () {
        this.event = '视频播放错误';
    },
    seekingCallback: function (e) {
        this.seekingtime = e.currenttime;
    },
    timeupdateCallback: function (e) {
        this.timeupdatetime = e.currenttime;
    },
    change_start_pause: function () {
        if (this.isStart) {
            this.$element(`playerId${this.player_index}`).pause();
            this.isStart = false;
        } else {
            this.$element(`playerId${this.player_index}`).start();
            this.isStart = true;
        }
    },
    change_fullscreenchange: function () { //全屏
        if (!this.isfullscreenchange) {
            this.$element(`playerId${this.player_index}`).requestFullscreen({
                screenOrientation: 'default'
            });
            this.isfullscreenchange = true;
        } else {
            this.$element(`playerId${this.player_index}`).exitFullscreen();
            this.isfullscreenchange = false;
        }
    },
    hiddendevice(){
        this.devicelist = "hidden";
    },
    initAction(code) {
        var actionData = {};
        actionData.firstNum = 1024;
        actionData.secondNum = 2048;
        var action = {};
        action.bundleName = "com.corecode.video.videoplayer";
        action.abilityName = "com.corecode.video.videoplayer.DeviceInternalAbility";
        action.messageCode = code;
        action.data = actionData;
        action.abilityType = 1;
        action.syncOption = 1;
        return action;
    },
    getLevel: async function () {
        try {
            var action = this.initAction(1001);
            var result = await FeatureAbility.callAbility(action);
            console.info(" result = " + result);
            this.deviceId = JSON.parse(JSON.parse(result).result);
            console.log("deviceId==" + this.deviceId)
            this.devicelist = "visible";
        } catch (pluginError) {
            console.error("getBatteryLevel : Plugin Error = " + pluginError);
        }
    }
}
